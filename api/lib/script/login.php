<?php
// ******************************************************
// * stripeuse4.0 for lapin.org                         *
// * this file is under GLPv3 or higher                 *
// * 2017 Quentin Pourriot <quentinpourriot@outlook.fr> *
// * 2017 Philippe Pary <philippe.pary@pachyderme.net>  *
// ******************************************************

function loginDomain($domain,$data) {
    $db = connectDb($domain);
    $query = $db->select(['id','login','name'])
                ->from('info')
                ->where('pwd','=',md5($data['pwd']));
    $exec = $query->execute();
    $data = $exec->fetchAll();
    return $data;
}

function login($data) {
    $db = connectDb();
    $query = $db->select()
                ->from('admin')
                ->where('login','=',$data['login'])
                ->where('pwd','=',md5($data['pwd']));
    $exec = $query->execute();
    $result = $exec->fetch();

    if ($result['pwd'] == md5($data['pwd'])) {
        return true;
    }
    else {
        echo 'incorrect login or password';
    }
}

function isSadmin($data) {
    error_log(implode(";",$data));
    if (login($data)) {
        $db = connectDb();
        $query = $db->select(array('id'))
                    ->from('admin')
                    ->where('login','=',$data['login']);
        $exec = $query->execute();
        $result = $exec->fetch();

        $query = $db->select()
                    ->from('s_admin')
                    ->where('id_admin','=',$result['id']);
        $exe = $query->execute();

        if (count($exe->fetch()) >= 2) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
?>
