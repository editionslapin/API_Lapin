<?php
// ******************************************************
// * stripeuse4.0 for lapin.org                         *
// * this file is under GLPv3 or higher                 *
// * 2017 Quentin Pourriot <quentinpourriot@outlook.fr> *
// * 2017 Philippe Pary <philippe.pary@pachyderme.net>  *
// ******************************************************


function addStrip($data) {
 $db = connectDb($data['domain']);

 if (storyIdExist($db,$data)) {
  $query = $db->insert(array('title','file','story_id','date'))
              ->into('strips')
              ->values(array(e($data['title']),e($data['file']),e($data['story_id']),e($data['date'])));
              if ($exe = $query->execute()) {
      return 'Strips correctement Enregistrer';
    }
    else {
      return 'Probleme d\'enregistrement du strips';
    }
  }
}

function addStory($data) {
  $db = connectDb(e($data['domain']));

  $query = $db->insert(array('title'))
              ->into('stories')
              ->values(array(e($data['title'])));
  if ($exe = $query->execute()) {
    return 'Story Enregistrer';
  }
  else {
    return 'Probleme d\'enregistrement de la storie';
  }
}

function addAdmin($data) {
  $db = connectDb();

  $query = $db->insert(array('name','login','pwd'))
              ->into('admin')
              ->values(array(e($data['name']),e($data['newLogin']),e($data['newPwd'])));
  $lastid = $db->lastInsertId();

  if ($exe = $query->execute()) {
    if ($data['sAdmin']) {
      $query = $db->insert(array('id_admin'))
                  ->into('s_admin')
                  ->values($lastid);
      if ($exe = $query->execute()) {
        echo 'S Admin enregistré';
      }
    }
    else {
      echo 'Admin enregistré';
    }
  }
}

function addPub($data) {
  if (isset($data['domain'])) {
    $db = connectDb($data['domain']);
  }
  else {
    $db = connectDb();
  }

  $query = $db->insert(array('name','file','link'))
              ->into('pub')
              ->values(array($data['name'],$data['file'],$data['link']));

  if ($exe = $query->execute()) {
    echo 'Pub enregistrer';
  }
  else {
    echo 'Probleme d\'enregistrement';
  }

}
 ?>
