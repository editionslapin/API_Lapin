<?php
// ******************************************************
// * stripeuse4.0 for lapin.org                         *
// * this file is under GLPv3 or higher                 *
// * 2017 Quentin Pourriot <quentinpourriot@outlook.fr> *
// ******************************************************
  //NUKE THE DATABASE
  function dropThebase($data){
    $db = connectSql();
    $sql = "DROP DATABASE " . $data['domain'];
    if($db->query($sql) == TRUE){
      echo 'Domain supprimé';
    }
    else {
      echo "Erreur de suppression";
    }
  }
  //DELETE STRIP
  function deleteStrips($data){
    $db = connectDb($data['domain']);
    if($data['all']){
      $sql = "TRUNCATE Table strips";
      if ($db->query($sql) === TRUE) {
        echo "Tout les Strips sont supprimés";
      } else {
        echo "Erreur de suppression";
      }
    }
    else{
      $query = $db->delete()
      ->from('strips')
      ->where('id','=',$data['id']);
      if($exe = $query->execute()){
        echo 'strip supprimé';
      }
    }
  }
  //DELETE STORIES (WITH STRIPS)
  function deleteStories($data){
      $db = connectDb($data['domain']);
      if (isset($data['id'])) {
          $query = $db->delete()
                      ->from('stories')
                      ->where('id','=',$data['id']);
      }
      else {
          $query = $db->delete()
                      ->from('stories');
      }
        if($exe = $query->execute()){
          echo 'stories supprimé';
        }
      if($data['withStrip']){
      $query = $db->delete()
                  ->from('strips')
                  ->where('story_id','=',$data['id']);
        if($exe = $query->execute()){
            echo 'strips supprimé';
        }
      }
  }
  function deleteAdmin($data){
    $db = connectDb();
    $query = $db->delete()
                ->from('admin')
                ->where('id','=',$data['id']);
    if($exe = $query->execute()){
      $query = $db->delete()
                  ->from('s_admin')
                  ->where('id_admin','=',$data['id']);
      $exe = $query->execute();
      echo 'Admin supprimé';
    }
  }
  function deletePub($data){
    if(isset($data['domain'])){
      $db = connectDb($data['domain']);
    }
    else{
      $db = connectDb();
    }
    $query = $db->delete()
                ->from('pub')
                ->where('id','=',$data['id']);
    if($exe = $query->execute()){
      echo 'Strips supprimé';
    }
  }
 ?>
