<?php
// ******************************************************
// * stripeuse4.0 for lapin.org                         *
// * this file is under GLPv3 or higher                 *
// * 2017 Quentin Pourriot <quentinpourriot@outlook.fr> *
// ******************************************************

namespace Lapin;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'sql/config-db.php';
require 'lib/loader.php';


$app = new \Slim\App([
  'settings' => [
      'addContentLengthHeader' => false,
      'displayErrorDetails' => false,
  ],
]);

$app->add(function ($req, $res, $next) {
      $response = $next($req, $res);
      return $response
          ->withHeader('Access-Control-Allow-Origin', '*')
          ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
          ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
          ->withHeader('X-Clacks-Overhead', 'GNU Terry Pratchett');
});

// *******************
// * SETTINGS NEW DB *
// *******************
$mwLoginAdmin = function ($request, $response, $next) {
    $dataLog = $request->getParsedBody();
    if(login($dataLog)){
      $response = $next($request, $response);
      return $response;
    }
};

$mwLoginSadmin = function ($request, $response, $next) {
    $dataLog = $request->getParsedBody();
    if(isSadmin($dataLog)){
      $response = $next($request, $response);
      return $response;
    }
};

/**
 * @rest\endpoint /
 * @rest\method GET Default page
 * @rest\response 200 string
 */
$app->any('/',function($request,$response, $args) {
    return $response->withRedirect('swagger-ui.html',303);
});

/**
 * @rest\endpoint /newDomain
 * @rest\method POST create a new domain
 * @rest\consumes json
 * @rest\body Domain body
 * @rest\response 200
 * @rest\tags super-admin
 */
$app->post('/newDomain',function($request, $response, $args){
  newDomain($request->getParsedBody());
})->add($mwLoginSadmin);

/**
 * @rest\endpoint /admin/getAdmin
 * @rest\method GET get a list of all admins
 * @rest\response 200 array(Admin) An array of admins
 * @rest\tags super-admin
 */
$app->post('/admin/getAdmin',function($request, $response, $args){
  $response = getAdmin();
  return $response;
})->add($mwLoginSadmin);

// **********
// * GETTER *
// **********

/**
 * @rest\endpoint /infoGeneral
 * @rest\method GET get the information of all comic strips
 * @rest\response 200
 */
$app->get('/infoGeneral',function($request, $response, $args){
  getAllInfo();
});

/**
 * @rest\endpoint /info/{domain}
 * @rest\method GET get the information of a comic strip
 * @rest\path String domain short_name of the domain
 * @rest\response 200 Domain
 * @rest\tags domain
 */
$app->get('/info/{domain}',function($request, $response, $args){
  getInfoByDomain($args['domain']);
});

/**
 * @rest\endpoint /strips/image/{domain}/{id}
 * @rest\method GET get the image of a strip
 * @rest\path String domain short_name of the domain
 * @rest\path Int id id of the strip
 * @rest\response 200 String base64 encoded image of the strip
 * @rest\tags strip
 */
$app->get('/strips/image/{domain}/{id}',function($request, $response, $args){
  getStripImage($args['domain'],$args['id']);
});

/**
 * @rest\endpoint /strips/since/{domain}/{date}
 * @rest\method GET get strips of a domain since a date
 * @rest\path String domain short_name of the domain
 * @rest\query? Date date date from which the strips have to be selected
 * @rest\response 200 array(Strip) array of strips definitions
 * @rest\tags strip
 */
$app->get('/strips/since/{domain}[/{date}]',function($request, $response, $args){
  getStripsByDate($args['domain'],$args['date']);
});

/**
 * @rest\endpoint /strips/stories/{domain}/{id}/{number}/{offset}
 * @rest\method GET get strips of a story
 * @rest\path String domain short_name of the domain
 * @rest\path Int id Id of the story
 * @rest\query? Int number Number of strip to get (default: 20)
 * @rest\query? Int offset Id of the first fetched strip
 * @rest\response 200 array(Strip) array of strips definitions
 * @rest\tags strip
 */
$app->get('/strips/stories/{domain}/{id}[/{number}[/{offset}]]',function($request, $response, $args){
  getStripsByStories($args['domain'],$args['id'],$args['number'],$args['offset']);
});

/**
 * @rest\endpoint /strips/{domain}/{number}/{offset}
 * @rest\method GET get strips of a domain
 * @rest\path String domain short_name of the domain
 * @rest\query? Int number Number of strip to get (default: 20)
 * @rest\query? Int offset Id of the first fetched strip
 * @rest\response 200 array(Strip) array of strips
 * @rest\tags strip
 */
$app->get('/strips/{domain}[/{number}[/{offset}]]',function($request, $response, $args){
  getStripsByDomain($args['domain'],$args['number'],$args['offset']);
});

/**
 * @rest\endpoint /stories/{domain}/{id}
 * @rest\method GET get a story
 * @rest\path String domain short_name of the domain
 * @rest\path Int id id of the story
 * @rest\response 200 Story A story
 * @rest\tags story
 */
$app->get('/stories/{domain}/{id}', function($request, $response, $args) {
  getStoryByDomain($args['domain'],$args['id']);
});

/**
 * @rest\endpoint /stories/{domain}/{number}/{offset}
 * @rest\method GET get stories of a domain
 * @rest\path String domain short_name of the domain
 * @rest\query? Int number Number of strip to get (default: 20)
 * @rest\query? Int offset Id of the first fetched strip
 * @rest\response 200 array(Strip) array of stories definitions
 * @rest\tags story
 */
$app->get('/stories/{domain}[/{number}/{offset}]',function($request, $response, $args){
  getStoriesByDomain($args['domain'],$args['number'],$args['offset']);
});

/**
 * @rest\endpoint /pub/general/{number}/{offset}
 * @rest\method GET get one or all general ads
 * @rest\query? Int number Number of strip to get (default: 1)
 * @rest\query? Int offset Id of the first fetched strip
 * @rest\response 200 array(Pub) An array of ads
 * @rest\tags pub
 */
$app->get('/pub/general[/{id}]', function($request, $response, $args){
  getLapinPub($args['id']);
});

/**
 * @rest\endpoint /pub/domain/{domain}/{number}/{offset}
 * @rest\method GET get one or all domain-specific ads
 * @rest\path String domain Short name of the domain
 * @rest\query? Int number Number of strip to get (default: 1)
 * @rest\query? Int offset Id of the first fetched strip
 * @rest\response 200 array(Pub) An array of ads
 * @rest\tags pub
 */
$app->get('/pub/domain/{domain}', function($request, $response, $args){
  getDomainPub($args['domain']);
});


// **********
// * SETTER *
// **********

/**
 * @rest\endpoint /admin/addAdmin
 * @rest\method POST add an admin
 * @rest\query String name Full name of the admin
 * @rest\query String login Login of the admin
 * @rest\query String Pwd MD5 password of the admin
 * @rest\response 200
 * @rest\tags super-admin
 */
$app->post('/admin/addAdmin',function($request, $response, $args){
  $response = addAdmin($request->getParsedBody());
  return $reponse;
})->add($mwLoginSadmin);

/**
 * @rest\endpoint /admin
 * @rest\method POST login as super-admin
 * @rest\body String login Admin login
 * @rest\body String pwd Admin password
 * @rest\response 200
 * @rest\tags super-admin
 */
$app->post('/admin',function($request, $response, $args){
  $response = isSadmin($request->getParsedBody());
  return $response;
});

/**
 * @rest\endpoint /{domain}/admin
 * @rest\method POST login as admin of a given domain
 * @rest\path String domain domain to log on
 * @rest\body String login Admin login
 * @rest\body String pwd Admin password
 * @rest\response 200
 * @rest\tags domain
 */
$app->post('/{domain}/admin', function($request, $response, $args){
    $response = loginDomain($args['domain'],$request->getParsedBody());
    if(count($response) == 1){
      return $response;
    }
});

/**
 * @rest\endpoint /strips/newStrip
 * @rest\method POST Add a new strip
 * @rest\consumes json
 * @rest\body Strip strip the comic strip to add
 * @rest\response 200
 * @rest\tags strip
 */
$app->post('/strips/newStrip',function($request, $response, $args){
  $response = addStrip($request->getParsedBody());
  return $response;
})->add($mwLoginAdmin);

/**
 * @rest\endpoint /strips/newStory
 * @rest\method POST Add a new story
 * @rest\consumes json
 * @rest\body Story the storyy to add
 * @rest\response 200
 * @rest\tags story
 */
$app->post('/stories/newStory',function($request, $response, $args){
  $response = addStory($request->getParsedBody());
  return $response;
})->add ($mwLoginAdmin);//OK

/**
 * @rest\endpoint /strips/addPub
 * @rest\method POST Add a new ad
 * @rest\consumes json
 * @rest\body Story the ad to add
 * @rest\response 200
 * @rest\tags pub
 */
$app->post('/pub/addPub/',function($request,$response,$args){
  addPub($request->getParsedBody());
})->add($mwLoginAdmin);//OK

//**********
//* DELETE *
//**********

/**
 * @rest\endpoint /delete/domain
 * @rest\method DELETE Deletes a domain
 * @rest\body String domain domain short name
 * @rest\response 200
 * @rest\tags domain
 */
$app->delete('/delete/domain', function($request, $response, $args){
  dropTheBase($request->getParsedBody());
})->add($mwLoginAdmin);

/**
 * @rest\endpoint /delete/stories
 * @rest\method DELETE Deletes a story or all 
 * @rest\body String domain domain short name
 * @rest\body? Int id story's id
 * @rest\body? Boolean withStrip Wether delete strips of the story or not
 * @rest\response 200
 * @rest\tags story
 */
$app->delete('/delete/stories', function($request, $response, $args){
  deleteStories($request->getParsedBody());
})->add($mwLoginAdmin);


/**
 * @rest\endpoint /delete/strips
 * @rest\method DELETE Deletes strips of a given domain
 * @rest\body String domain domain short name
 * @rest\body? Boolean all If set to true, deletes all strips
 * @rest\body? Int id Id of the strip to delete
 * @rest\response 200
 * @rest\tags strip
 */
$app->delete('/delete/strips',function($request, $response, $args){
  deleteStrips($request->getParsedBody());
})->add($mwLoginAdmin);

/**
 * @rest\endpoint /delete/admin
 * @rest\method DELETE Deletes an admin
 * @rest\body Int id Id of the admin
 * @rest\response 200
 * @rest\tags super-admin
 */
$app->delete('/delete/admin',function($request, $response, $args){
  deleteAdmin($request->getParsedbody());
})->add($mwLoginSadmin);

/**
 * @rest\endpoint /delete/pub
 * @rest\method DELETE Deletes an ad
 * @rest\body? String domain domain's short name. Without, you try to delete a global ad
 * @rest\body Int id id of the ad to delete
 * @rest\response 200
 * @rest\tags pub
 */
$app->delete('/delete/pub',function($request, $response, $args){
  deletePub($request->getParsedbody());
})->add ($mwLoginAdmin);//OK

//**********
//* UPDATE *
//**********

/**
 * @rest\endpoint /update/domain
 * @rest\method PUT Update a domain
 * @rest\consumes json
 * @rest\body Domain domain Domain to update
 * @rest\response 200
 * @rest\tags domain
 */
$app->put('/update/domain', function($request,$response, $args){
  updateInfo($request->getParsedBody());
})->add($mwLoginAdmin);//Ok

/**
 * @rest\endpoint /update/strip
 * @rest\method PUT Update a strip
 * @rest\consumes json
 * @rest\body Strip strip Strip to update
 * @rest\response 200
 * @rest\tags strip
 */
$app->put('/update/strip', function($request,$response,$args){
  updateStrips($request->getParsedBody());
})->add($mwLoginAdmin);

/**
 * @rest\endpoint /update/story
 * @rest\method PUT Update a story
 * @rest\consumes json
 * @rest\body Story story Story to update
 * @rest\response 200
 * @rest\tags story
 */
$app->put('/update/story', function($request,$response,$args){
  updateStories($request->getParsedBody());
})->add($mwLoginAdmin);

/**
 * @rest\endpoint /update/admin
 * @rest\method PUT Update an admin
 * @rest\consumes json
 * @rest\body Admin admin Admin to update
 * @rest\response 200
 * @rest\tags super-admin
 */
$app->put('/update/admin',function($request, $response, $args){
  updateAdmin($request->getParsedBody());
})->add($mwLoginSadmin);

/**
 * @rest\endpoint /update/pub
 * @rest\method PUT Update an ad
 * @rest\consumes json
 * @rest\body? String domain domain's short name. Without, you try to delete a global ad
 * @rest\body Int id id of the ad to delete
 * @rest\body String name name of the add
 * @rest\body String file base64 of image of the ad
 * @rest\body String link URL fo the ad
 * @rest\response 200
 * @rest\tags pub
 */
$app->put('/update/pub',function($request,$response,$args){
  updatePub($request->getParsedBody());
})->add($mwLoginAdmin);

/**
 * @rest\title Lapin API
 * @rest\description SwaggerGen 2 Lapin API
 * @rest\contact Philippe Pary philippe@pachyderme.net
 * @rest\license gpl
 * @rest\version 1
 */

/**
 * @rest\model Domain
 * @rest\description A domain is a comic strip : a set of strips by a given author
 * @rest\property String short_name Short name of the comic strip, which is also its id
 * @rest\property String large_name Full name of the comic strip
 * @rest\property String author Author's name
 * @rest\property String favicon base64 of favicon of the strip
 * @rest\property String file ONLY FOR MODIFICATION OR CREATION base64 encoded image of the strip
 */

/**
 * @rest\model Story
 * @rest\description A story is a set of strips
 * @rest\property String title Title of the story
 * @rest\property Int id Id of the story
 */

/**
 * @rest\model Strip
 * @rest\description A strip
 * @rest\property String title Title of the strip
 * @rest\property Int story_id Id of the story of the strip
 * @rest\property Date date Date of publication of the strip
 * @rest\property Int id Id of the strip
 * @rest\property String file ONLY FOR MODIFICATION OR CREATION base64 encoded image of the strip
 */

/**
 * @rest\model Admin
 * @rest\description An administrator of a domain (aka comic strip)
 * @rest\property String name Full name of the admin
 * @rest\property String login Short name for auth of the admin
 * @rest\property Int id Id of the admin
 */

/**
 * @rest\model Pub
 * @rest\description An ad
 * @rest\property String name Name of the ad
 * @rest\property String file base64 encoded image of the ad
 * @rest\property String link URL of the ad
 * @rest\property Int id Id of the ad
 */

$app->run();
?>
