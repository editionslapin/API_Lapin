<?php
require(__DIR__ . "/../vendor/autoload.php");

$SwaggerGen = new \SwaggerGen\SwaggerGen(
    $_SERVER['HTTP_HOST'],
    dirname($_SERVER['REQUEST_URI'])
);
error_log(dirname($_SERVER['REQUEST_URI']));
$swagger = $SwaggerGen->getSwagger(['index.php'], array(), \SwaggerGen\SwaggerGen::FORMAT_JSON_PRETTY);

header('Content-type: application/json');
echo $swagger;
file_put_contents("swagg.json", $swagger);

